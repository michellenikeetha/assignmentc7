#include <stdio.h>
#include <string.h>

int main()
{
	char arr[100];
	int c,i,count=0;
	int len;
	
	printf("Enter a sentence: \n ");
	fgets(arr,100,stdin);
	
	printf("Enter a character to find the character\n");
	scanf("%c", &c);
	
	len = strlen(arr);
	for(i=0; i<=len;i++)
		if(arr[i]==c)
		{
			count+=1;
		}
	printf("The frequency of %c is %d", c, count);	
	return 0;
}
