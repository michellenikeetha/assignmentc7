#include <stdio.h>

int main()
{
	int a[5][5], b[5][5], c[5][5]={0}, d[5][5]={0};
	int e,f,g,h,i,j,k;
	
	printf("Enter the number of rows and columns in the first matrix \n");
	scanf("%d %d", &e, &f);
	
	printf("Enter the number of rows and columns in the second matrix \n");
	scanf("%d %d", &g, &h);
	
	if(e!=g || f!=h)
		printf("Matrix addition cannot be performed due to not matching rows or columns \n");
		
	else if(f!=g)
		printf("Matrix multiplication cannot be performed due to not matching rows or columns \n");
		
	else
	{
		printf("Enter the elements of first matrix \n");
		for (i=0; i<e; i++)
		{
			for (j=0; j<f; j++)
				scanf("%d", &a[i][j]);
			}
		printf("\n");	
			
		printf("Enter the elements of second matrix \n");
		for (i=0; i<g; i++)
		{
			for (j=0; j<h; j++)
				scanf("%d", &b[i][j]);
			}	
		printf("\n");
		
		for(i=0; i<e; i++)
		{
			for(j=0; j<f; j++)
				c[i][j]=a[i][j]+b[i][j];
		}
		
		printf("Result of matrix addition\n");
		for(i=0; i<e; i++)
		{
			for(j=0; j<f; j++)
				printf("%d ", c[i][j]);
			printf("\n");
		}
		
		for(i=0;i<e;i++)
		{
			for(j=0;j<f;j++)
			{
				for(k=0;k<e;k++)
					d[i][j]=d[i][j] + a[i][k]*b[k][j];	
			}
		}
		
		printf("Result of matrix multiplication\n");
		for(i=0;i<e;i++)
		{
			for(j=0;j<f;j++)
				printf("%d ", d[i][j]);
			printf("\n");
		}		
	return 0;
	}
}
