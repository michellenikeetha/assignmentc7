#include <stdio.h>
#include <string.h>

int main()
{
	char arr[100];
	int i;
	int len;
	
	printf("Enter a sentence: \n ");
	fgets(arr,100,stdin);
	
	len = strlen(arr);
	
	printf("The reverse sentence is :\n" );
	for (i=len; i>=0; i-- )
		printf("%c",arr[i]);
	return 0;
}
 
